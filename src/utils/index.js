import { 
  Check, Clear,FirstPage, LastPage, 
  ChevronLeft, ChevronRight, Search,
  Remove, Delete, Favorite, 
} from "@material-ui/icons";

export const icons = {
  Add: Favorite,
  Delete,
  Check,
  ResetSearch: Clear,
  FirstPage,
  LastPage,
  NextPage: ChevronRight,
  PreviousPage: ChevronLeft,
  Search,
  ThirdStateCheck: Remove,
};