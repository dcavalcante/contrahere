import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import App from './components/App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// import ApolloClient from 'apollo-boost';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';

import { ApolloProvider } from 'react-apollo';
import { createUploadLink } from 'apollo-upload-client';
import AddContract from './components/AddContract';
import EditContract from './components/EditContract';
import Parties from './components/Parties';
import EditParty from './components/EditParty';

// const client = new ApolloClient({
//   uri: "http://localhost:4000/graphql",
//   link: createUploadLink({
//     uri: "http://localhost:4000/graphql",
//   }),
// });

const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.forEach(({ message, locations, path }) =>
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
          ),
        );
      if (networkError) console.log(`[Network error]: ${networkError}`);
    }),
    createUploadLink({
      uri: 'http://localhost:4000/graphql',
    })
    // new HttpLink({
    //   uri: 'http://localhost:4000/graphql',
    //   // credentials: 'cors'
    // })
  ]),
  cache: new InMemoryCache()
});

ReactDOM.render(
  <>
  <ApolloProvider client={client}>
    <Router>
        <div>
          <Route exact path='/' component={App} />
          <Route exact path='/add/contract' component={AddContract} />
          <Route path='/contract/:id' component={EditContract} />
          <Route exact path='/parties' component={Parties} />
          <Route path='/party/:id' component={EditParty} />
          {/*<Route path='/create' component={Create} />
          <Route path='/show/:id' component={Show} /> */}
      </div>
    </Router>
  </ApolloProvider>
  </>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
