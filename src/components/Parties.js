import React, { Component } from 'react';
import { Link, Redirect, withRouter } from 'react-router-dom';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import MaterialTable from 'material-table';
import Button from '@material-ui/core/Button';
import AddParty from './AddParty';
import {icons} from '../utils';

const GET_PARTIES = gql`
  query parties {
    parties {
      id
      firstname
      lastname
      email
      cpf
      phone
    }
  }
`;

const REMOVE_PARTIES = gql`
  mutation RemoveParties($ids:[String!]!) {
    partiesRemove(
      ids: $ids,
    ) {
      id
    }
  }
`;



// export default function Contracts(props) {
const Parties = withRouter(({history}) => {
  
  return (
    <Query 
      // pollInterval={500} 
      query={GET_PARTIES}
    >
      {({loading, error, data, refetch}) => {
        if (loading) return "Carregando...";
        if (error) return `Erro: ${error.message}`;
        console.log(data);
        return (
          <div className="contracts">
            <AddParty refetch={refetch} />
            <Mutation 
              mutation={REMOVE_PARTIES}
              onCompleted={result => {console.log(result); refetch();}}
            >
            {(RemoveParties, {loading, error}) => (
              <MaterialTable
                title="Partes"
                icons={icons}
                columns={[
                  {title: "Nome", field: "firstname"},
                  {title: "Sobrenome", field: "lastname"},
                  {title: "Email", field: "email"},
                  {title: "CPF", field: "cpf"},
                  {title: "Telefone", field: "phone"},
                  {
                    title: "Editar",
                    field: "id",
                    render: row => (
                      <Button
                        color="secondary"
                        variant="contained"
                        style={{textTransform: 'none'}}
                        onClick={evt => {
                          console.log(row)
                          history.push(`/party/${row.id}`);
                        }}
                      >
                        Editar
                      </Button>
                    )
                  },
                ]}
                actions={[
                  {
                    icon: 'remove', 
                    tooltip: 'Remove Parties',
                    onClick: (evt, data) => {
                      console.log(data)
                      const ids = data.map(row => row.id);
                      console.log(ids)
                      RemoveParties({variables:{ids}})
                    }
                  }
                ]}
                data={data.parties}
                options={{
                  sorting: true,
                  selection: true,
                  padding: "dense",
                }}
                // components={{
                //   Action: props => (
                //     <Button
                //       onClick={(event) => props.action.onClick(event, props.data)}
                //       color="primary"
                //       variant="contained"
                //       style={{textTransform: 'none'}}
                //       size="small"
                //     >
                //       Delete
                //     </Button>
                //   ),
                // }}
              />
            )}
            </Mutation>
          </div>
        );
      }}
    </Query>
  );
});
export default Parties;