import React, { useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const GET_PARTY = gql`
  query party($id: ID!) {
    party(id: $id) {
      id
      firstname
      lastname
      email
      cpf
      phone
    }
  }
`;

const UPDATE_PARTY = gql`
  mutation UpdateParty(
    $id:ID!, $firstname:String, 
    $lastname:String, $email:String, 
    $cpf:String, $phone:String, 
  ) {
    partyUpdate(
      id:$id,
      firstname:$firstname,
      lastname:$lastname,
      email:$email,
      cpf:$cpf,
      phone:$phone,
    ) {
      id
      firstname
      lastname
      email
      cpf
      phone
    } 
  }
`;

// function EditParty(props) {
 const EditParty = withRouter((props) => {

  console.log(props);
  const {id} = props.match.params;
  console.log(id);

  const [form, setForm] = useState({firstname:undefined});
  console.log(form);
  return (
    <Query query={GET_PARTY} variables={{id}}>
      {({loading, error, data}) => {
        if (loading) return "Loading...";
        if (error) return `Error: ${error.message}`;
        console.log(data)
        if (!form.firstname) {setForm(data.party)}
        console.log(form);

        return (
          <Mutation 
            mutation={UPDATE_PARTY}
            onCompleted={result => {props.history.push('/parties')}}
          >
            {(UpdateParty, {loading, error}) => (
              <form onSubmit={e => {
                e.preventDefault();
                UpdateParty({variables: {
                  id: form.id,
                  firstname: form.firstname,
                  lastname: form.lastname,
                  email: form.email,
                  cpf: form.cpf,
                  phone: form.phone,

                }})
              }}>
                <TextField 
                  required placeholder="Nome" 
                  value={form.firstname ? form.firstname : data.party.firstname}
                  onChange={e => setForm({...form, firstname:e.target.value})}
                />
                <TextField 
                  required placeholder="Sobrenome" 
                  value={form.lastname ? form.lastname : data.party.lastname}
                  onChange={e => setForm({...form, lastname:e.target.value})}
                />
                <TextField 
                  required placeholder="Email" 
                  type="email"
                  value={form.email ? form.email : data.party.email}
                  onChange={e => setForm({...form, email:e.target.value})}
                />
                <TextField 
                  required placeholder="CPF" 
                  value={form.cpf ? form.cpf : data.party.cpf}
                  type="number"
                  onChange={e => setForm({...form, cpf:e.target.value})}
                />
                <TextField 
                  required placeholder="Telefone" 
                  type="number"
                  value={form.phone ? form.phone : data.party.phone}
                  onChange={e => setForm({...form, phone:e.target.value})}
                />
                 
                <Button 
                  type="submit" 
                  variant="contained"
                  color="primary"
                  margin="normal"
                  >
                  Atualizar
                </Button>        
              </form>
            )}
          </Mutation>
        );
      }}
    </Query>
  );
});

export default EditParty;