import React, { useState } from 'react';
import { Link, Redirect, withRouter } from 'react-router-dom';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

const ADD_PARTY = gql`
  mutation AddParty(
    $firstname:String!,
    $lastname:String!,
    $email:String!,
    $cpf:String!,
    $phone:String!,
    
  ) {
    partyAdd(
      firstname: $firstname, 
      lastname: $lastname, 
      email: $email, 
      cpf: $cpf, 
      phone: $phone, 
      
    ) {
      id
      firstname
      lastname
      email
      cpf
      phone
    }
  }
`
const useStyles = makeStyles(theme => ({
  margin: {
    margin: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    margin: "20px auto",
  },
}));

function AddParty(props) {
  const classes = useStyles();

  const [form, setForm] = useState({
    firstname:'',
    lastname:'',
    email:'',
    cpf:'',
    phone:'',
  });
  const {refetch} = props;
  console.log(form)
  return (
    <Mutation 
      mutation={ADD_PARTY}
      onCompleted={result => {console.log(result); refetch();}}
    >
      {(AddParty, {loading, error}) => (
        <Grid container>
          <Paper className={classes.paper}>
            <Typography variant="h4" element="h1" align="center">
              Adicionar Parte
            </Typography>
            <form
              onSubmit={e => {
                e.preventDefault();
                console.log(form)
                AddParty({variables:{
                  firstname: form.firstname,
                  lastname: form.lastname,
                  email: form.email,
                  cpf: form.cpf.toString(),
                  phone: form.phone.toString(),
                }});
                setForm({
                  firstname:'',
                  lastname:'',
                  email:'',
                  cpf:'',
                  phone:'',
                });
              }}
            >
              <Grid item>
                <TextField
                  required placeholder="Nome" 
                  margin="normal"
                  value={form.firstname}
                  className={classes.margin}
                  onChange={e => setForm({...form, firstname:e.target.value})}
                />
                <TextField
                  required placeholder="Sobrenome" 
                  margin="normal"
                  value={form ? form.lastname : ''}
                  className={classes.margin}
                  onChange={e => setForm({...form, lastname:e.target.value})}
                />
              </Grid>
              <Grid item>
                <TextField
                  required placeholder="Email" 
                  margin="normal" 
                  type="email"
                  value={form ? form.email : ''}
                  className={classes.margin}
                  onChange={e => setForm({...form, email:e.target.value})}
                />
                <TextField
                  required placeholder="CPF" 
                  margin="normal" 
                  type="number"
                  inputProps={{maxLength:12, minLength:12}}
                  value={form ? form.cpf : ''}
                  className={classes.margin}
                  onChange={e => setForm({...form, cpf:e.target.value.slice(0,12)})}
                />
                <TextField
                  required placeholder="Telefone" 
                  margin="normal" 
                  type="number"
                  value={form ? form.phone : ''}
                  className={classes.margin}
                  onChange={e => setForm({...form, phone:e.target.value})}
                />
              </Grid>
              <Grid item>

                
                <Button 
                  type="submit" 
                  variant="contained"
                  color="primary"
                  margin="normal"
                  className={classes.margin}
                  >
                  Salvar
                </Button>
              </Grid>
            </form>
          </Paper>
        </Grid>
      )}
    </Mutation>
  )
}

export default AddParty;