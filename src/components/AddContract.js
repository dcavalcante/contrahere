import React, { useState, useRef } from 'react';
import { withRouter } from 'react-router-dom';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { useFormState } from 'react-use-form-state';
import DateFnsUtils from '@date-io/date-fns';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';

const GET_PARTIES = gql`
  query parties {
    parties {
      id
      firstname
      lastname
      email
      cpf
      phone
    }
  }
`;

const ADD_CONTRACT = gql`
  mutation AddContract(
    $title:String!,
    $startDate:DateTime!,
    $endDate:DateTime!,
    $pdf:Upload!,
    $parties:[ID]
  ) {
    contractAdd(
      title: $title, 
      startDate: $startDate,
      endDate: $endDate, 
      parties: $parties, 
      pdf: $pdf,
    ) {
      id
      title
      startDate
      endDate
      pdf
      parties
    }
  }
`

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    margin: "20px auto",
  },
  margin: {
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(2),
  }
}));

// function AddContract() {
const AddContract = withRouter(({history}) => {
  const classes = useStyles();
  const pdf = useRef(null);
  const rawDates = { 
    startDate: new Date(),
    endDate: new Date(),
  }
  const [formState, {text, raw}] = useFormState(rawDates);

  const [parties, setParties] = useState([]);
  function handleSelect(e) {
    console.log(e);
    const { value } = e.target;
    const result = [];
    for (let i = 0, l = value.length; i < l; i += 1) {
      if (value[i].parties) {
        result.push(value[i]);
      }
      result.push(value[i]);
    }
    setParties(result);
  }
  console.log(parties);
  return (
    <Mutation 
      mutation={ADD_CONTRACT}
      onCompleted={result => history.push('/')}
    >
      {(AddContract, {loading, error}) => (
        <Grid container>
          <Paper className={classes.paper}>
            <Typography variant="h4" element="h1" align="center">
              Adicionar Contrato
            </Typography>
            <form
              onSubmit={e => {
                e.preventDefault();
                console.log(formState)
                const variables = {
                  title: formState.values.title,
                  startDate: formState.values.startDate.toISOString(),
                  endDate: formState.values.endDate.toISOString(),
                  pdf:pdf.current.files[0], parties,
                };
                console.log(variables)
                AddContract({variables});
                // history.push('/');
              }}
            >
              <Grid item>
                <TextField {...text('title')} 
                  required placeholder="Título" 
                  fullWidth={true}
                  margin="normal" 
                />
              </Grid>
              <Grid item>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="Início"
                    format="MM/dd/yyyy"
                    value={Date.now()}
                    // onChange={handleDateChange}
                    KeyboardButtonProps={{
                      'aria-label': 'Data de inicio',
                    }}
                    {...raw('startDate')}
                  />
                  <KeyboardDatePicker
                    margin="normal"
                    id="date-picker-dialog"
                    label="Término"
                    format="MM/dd/yyyy"
                    // value={selectedDate}
                    // onChange={handleDateChange}
                    KeyboardButtonProps={{
                      'aria-label': 'change end date',
                    }}
                    {...raw('endDate')}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item>

                Partes:
                <Query 
                  query={GET_PARTIES}
                >
                  {({loading, error, data, refetch}) => {
                  console.log(data)
                  if (loading) return "Loading..."
                  return (
                    <Select
                      multiple
                      onChange={handleSelect}
                      value={parties}
                    >
                      {data.parties.map(party => {
                        return (
                          <MenuItem key={party.id} value={party.id}>
                            {party.firstname + ' ' + party.lastname}
                          </MenuItem>
                        );
                      })}
                    </Select>
                    
                  )
                  }}
                </Query>
              </Grid>
              <input type="file"
                ref={pdf}
                onChange={e => console.log(e, pdf)}
              />
              <Button href="/parties"
                variant="contained"
                color="default"
                margin="normal"
                className={classes.margin}                
              >
                Gerenciar Partes
              </Button>
              <Button 
                type="submit" 
                variant="contained"
                color="primary"
                margin="normal"
                className={classes.margin}
                >
                Salvar
              </Button>
            </form>
          </Paper>
        </Grid>
      )}
    </Mutation>
  )
});

export default AddContract;