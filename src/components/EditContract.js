import React, { useState, useRef } from 'react';
import { withRouter } from 'react-router-dom';
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import { useFormState } from 'react-use-form-state';
import DateFnsUtils from '@date-io/date-fns';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';

const GET_PARTIES = gql`
  query parties {
    parties {
      id
      firstname
      lastname
      email
      cpf
      phone
    }
  }
`;

const GET_CONTRACT = gql`
  query contract($id: ID!) {
    contract(id: $id) {
      id
      title
      startDate
      endDate
      parties
      filename
    }
  }
`;

const UPDATE_CONTRACT = gql`
  mutation UpdateContract(
    $id:ID!, $title:String, 
    $startDate:DateTime, $endDate:DateTime,
    $pdf:Upload,
    $parties:[ID]
  ) {
    contractUpdate(
      id:$id,
      title:$title,
      startDate:$startDate,
      endDate:$endDate,
      pdf: $pdf,
      parties: $parties, 
    ) {
      id
      title
      pdf
      parties
      startDate
      endDate
    } 
  }
`;

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    margin: "20px auto",
  },
  margin: {
    marginRight: theme.spacing(2),
    marginTop: theme.spacing(2),
  }
}));

// function EditContract(props) {
const EditContract = withRouter((props) => {
  const classes = useStyles();
  const pdf = useRef(null);
  console.log(props);
  const {id} = props.match.params;
  console.log(id);

  const [form, setForm] = useState({title:undefined});
  const [parties, setParties] = useState();
  const [file, setFile] = useState(false);
  function handleSelect(e) {
    console.log(e);
    const { value } = e.target;
    const result = [];
    for (let i = 0, l = value.length; i < l; i += 1) {
      if (value[i].parties) {
        result.push(value[i]);
      }
      result.push(value[i]);
    }
    setParties(result);
  }

  console.log(form);
  return (
    <Query query={GET_CONTRACT} variables={{id}}>
      {({loading, error, data}) => {
        if (loading) return "Loading...";
        if (error) return `Error: ${error.message}`;
        console.log(data)
        if (!form.title) {setForm(data.contract)}
        if (!parties) {setParties(data.contract.parties)}
        console.log(form);
        console.log(parties);

        return (
          <Mutation 
            mutation={UPDATE_CONTRACT}
            onCompleted={result => props.history.push('/')}
          >
            {(UpdateContract, {loading, error}) => (
              <Grid container>
                <Paper className={classes.paper}>
                  <Typography variant="h4" element="h1" align="center">
                    Editar Contrato
                  </Typography>
                  <form onSubmit={e => {
                    e.preventDefault();
                    UpdateContract({variables: {
                      id: form.id,
                      title: form.title,
                      startDate: form.startDate,
                      endDate: form.endDate,
                      parties,
                      pdf: file ? pdf.current.files[0] : null,
                    }})
                  }}>
                    <Grid item>  
                      <TextField 
                        required placeholder="Title" 
                        value={form.title ? form.title : data.contract.title}
                        onChange={e => setForm({...form, title:e.target.value})}
                      />
                    </Grid>
                    <Grid item>  

                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                          margin="normal"
                          id="date-picker-dialog"
                          label="Início"
                          format="MM/dd/yyyy"
                          value={data.contract.startDate}
                          onChange={d => setForm({...form, startDate:d.toISOString()})}
                          KeyboardButtonProps={{
                            'aria-label': 'change start date',
                          }}
                          // {...raw('startDate')}
                        />
                      <KeyboardDatePicker
                          margin="normal"
                          id="date-picker-dialog"
                          label="Término"
                          format="MM/dd/yyyy"
                          value={data.contract.endDate}
                          onChange={d => setForm({...form, endDate:d.toISOString()})}
                          KeyboardButtonProps={{
                            'aria-label': 'change start date',
                          }}
                          // {...raw('startDate')}
                        />
                      </MuiPickersUtilsProvider>   
                    </Grid>
                    <Grid item>  
                      Partes: 
                      <Query 
                        query={GET_PARTIES}
                      >
                        {({loading, error, data, refetch}) => {
                        console.log(data)
                        if (loading) return "Loading..."
                        return (
                          <Select
                            multiple
                            onChange={handleSelect}
                            value={parties || []}
                          >
                            {data.parties.map(party => {
                              return (
                                <MenuItem key={party.id} value={party.id}>
                                  {party.firstname + ' ' + party.lastname}
                                </MenuItem>
                              );
                            })}
                          </Select>
                        )
                        }}
                      </Query>
                    </Grid>
                    {!file ?
                    <Grid item>
                      
                      <a className={classes.margin} 
                        href={`http://localhost:4000/uploads/${data.contract.id}.pdf`}>
                        Anexo
                      </a>
                      <Button
                        color="secondary"
                        variant="contained"
                        margin="normal"
                        onClick={e => setFile(true)}
                      >
                        Remover
                      </Button>
                    </Grid>
                    :
                    <Grid item>
                    <input type="file"
                      ref={pdf}
                      onChange={e => console.log(e, pdf)}
                    />
                    </Grid>
                    }
                    <Button href="/parties"
                      variant="contained"
                      color="default"
                      margin="normal"
                      className={classes.margin}
                    >
                      Gerenciar Partes
                    </Button>
                    <Button 
                      type="submit" 
                      variant="contained"
                      color="primary"
                      margin="normal"
                      className={classes.margin}
                      >
                      Atualizar
                    </Button>        
                  </form>
                </Paper>
              </Grid>
            )}
          </Mutation>
        );
      }}
    </Query>
  );
});

export default EditContract;