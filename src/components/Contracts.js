import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import MaterialTable from 'material-table';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {icons} from '../utils';

const GET_CONTRACTS = gql`
  query contracts {
    contracts {
      id
      title
      startDate
      endDate
      filename
      parties
    }
  }
`;

const REMOVE_CONTRACTS = gql`
  mutation RemoveContracts($ids:[String!]!) {
    contractsRemove(
      ids: $ids,
    ) {
      id
    }
  }
`;

const useStyles = makeStyles(theme => ({
  margin: {
    marginRight: theme.spacing(2),
    marginBottom: theme.spacing(2),
  }
}));

// export default function Contracts(props) {
const Contracts = withRouter(({history}) => {
  const classes = useStyles();
  
  return (
    <Query 
      query={GET_CONTRACTS}
    >
      {({loading, error, data, refetch}) => {
        if (loading) return "Carregando...";
        if (error) return `Erro: ${error.message}`;
        return (
          <div className="contracts">
            <Button href="/add/contract"
              variant="contained"
              color="primary"
              margin="normal"
              className={classes.margin}
            >
              Adicionar Contrato
            </Button>
            <Button href="/parties"
              variant="contained"
              color="default"
              margin="normal"
              className={classes.margin}
            >
              Gerenciar Partes
            </Button>
            <Mutation 
              mutation={REMOVE_CONTRACTS}
              onCompleted={result => {console.log(result); refetch();}}
            >
            {(RemoveContracts, {loading, error}) => (
              <MaterialTable
                title="Contratos"
                icons={icons}
                columns={[
                  {title: "Título", field: "title"},
                  {
                    title: "Início", 
                    field: "startDate", 
                    type: "date",
                    render: row => row.startDate.slice(0, 10),
                  },
                  {
                    title: "Término", 
                    field: "endDate", 
                    type: "date",
                    render: row => row.startDate.slice(0, 10),
                  },
                  {
                    title: "PDF",
                    field: "filename",
                    render: row => (
                      <a href={`http://localhost:4000/uploads/${row.id}.pdf`}>
                        Anexo
                      </a>
                    )
                  },
                  {
                    title: "Editar",
                    field: "id",
                    render: row => (
                      <Button
                        color="secondary"
                        variant="contained"
                        style={{textTransform: 'none'}}
                        onClick={evt => {
                          console.log(row)
                          history.push(`/contract/${row.id}`);
                        }}
                      >
                        Editar
                      </Button>
                    )
                  },
                ]}
                actions={[
                  {
                    icon: 'remove', 
                    tooltip: 'Remover Contrato',
                    onClick: (evt, data) => {
                      console.log(data)
                      const ids = data.map(row => row.id);
                      console.log(ids)
                      RemoveContracts({variables:{ids}})
                    }
                  }
                ]}
                data={data.contracts}
                options={{
                  sorting: true,
                  selection: true,
                  padding: "dense",
                }}
              />
            )}
            </Mutation>
          </div>
        );
      }}
    </Query>
  );
});
export default Contracts;