import React, {useState, useEffect} from 'react';
import Contracts from './Contracts';


function App() {
  return (
    <div className="App">
      <Contracts />
    </div>
  );
}

export default App;
